# base image
FROM node:10.16.3-alpine

# set working directory
COPY . ./docker-web-app
WORKDIR /docker-web-app

# install and cache app dependencies
RUN npm install
RUN npm install -g @angular/cli

# Enable API for dockerd
CMD touch /etc/systemd/system/docker.service.d/override.conf
CMD echo -e "[Service]\nExecStart=\nExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376 --api-cors-header=\"*\"" > /etc/systemd/system/docker.service.d/override.conf

# start app
CMD ng serve --host 0.0.0.0


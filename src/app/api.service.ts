import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  conn = this.getConnectionString();
  constructor(private httpClient: HttpClient) { }

  private getConnectionString() {
    this.conn = JSON.parse(localStorage.getItem('conn'));
    if (!this.conn) {
      localStorage.setItem('conn', JSON.stringify({"ip": "0.0.0.0", "port": "2376"}));
      this.conn = JSON.parse(localStorage.getItem('conn'));
    }
    return this.conn
  }

  public getImages(){
    return this.httpClient.get(`http://${this.conn.ip}:${this.conn.port}/v1.24/images/json?all=1`);
  }

  public getContainers(all){
    return this.httpClient.get(`http://${this.conn.ip}:${this.conn.port}/v1.24/containers/json?all=${all}`);
  }

  public startContainer(id){
	  return this.httpClient.post(`http://${this.conn.ip}:${this.conn.port}/v1.24/containers/${id}/start`, null)
	}

	public stopContainer(id){
	  return this.httpClient.post(`http://${this.conn.ip}:${this.conn.port}/v1.24/containers/${id}/stop`, null)
	}

	public deleteContainer(id){
	  return this.httpClient.delete(`http://${this.conn.ip}:${this.conn.port}/v1.24/containers/${id}`)
	}

  public inspectContainer(id) {
    return this.httpClient.get(`http://${this.conn.ip}:${this.conn.port}/v1.24/containers/${id}/json`)
  }

  public createContainer(obj) {
    return this.httpClient.post(`http://${this.conn.ip}:${this.conn.port}/v1.24/containers/create`, obj)
  }

  public getInfo() {
    return this.httpClient.get(`http://${this.conn.ip}:${this.conn.port}/v1.24/info`)
  }
}
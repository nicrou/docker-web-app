export class ContainerConfigWrapper {
	constructor(
		public Image: string,
		public Hostname?: string,
		public User?: string,
		public Env?: null,
		public Dns?: null,
		public Memory: number = 0,
		public MemorySwap: number = 0,
		public AttachStdin: boolean = false,
		public AttachStdout: boolean = true,
		public AttachStderr: boolean = true,
		public StdinOnce: boolean = false,
    public Privileged: boolean = false,
    public Tty: boolean = false,
    public OpenStdin: boolean = false,
  ) {  }
}

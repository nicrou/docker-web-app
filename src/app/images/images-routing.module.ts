import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageListComponent } from './image-list/image-list.component';
import { ContainerFormComponent } from '../containers/container-form/container-form.component';

const imagesRoutes: Routes = [
  { path: 'images',  component: ImageListComponent },
  { path: 'image/:id', component: ContainerFormComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(imagesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ImagesRoutingModule { }
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-images',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {
  images;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  	this.apiService.getImages().subscribe((response) => {
      // console.log(response);
      this.images = response;
    });
  }

  refetch(e: any) {
    this.apiService.getImages().subscribe((response) => {
      this.images = response;
    });
  }
}
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.scss']
})

export class ImageDetailComponent implements OnInit {
  @Input() image: any; RepoTags: Array <string>; Labels: any;
  @Output() refetch: EventEmitter <any> = new EventEmitter <any> ();

  constructor(private router: Router) { }

  ngOnInit() {
  	this.image.Id = this.image.Id.substring(7, 19);
  	this.RepoTags = this.image.RepoTags;
  	this.Labels = this.image.Labels;
  }
}

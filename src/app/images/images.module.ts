import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { ImageListComponent }    from './image-list/image-list.component';
import { ImageDetailComponent } from './image-detail/image-detail.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialDesignModule } from '../material-design/material-design.module';
import { ImagesRoutingModule } from './images-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ImagesRoutingModule,
    MaterialDesignModule,
    FlexLayoutModule
  ],
  declarations: [
    ImageListComponent,
    ImageDetailComponent,
  ]
})
export class ImagesModule {}
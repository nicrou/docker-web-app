import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  info;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  	this.info = JSON.parse(localStorage.getItem('info'));
  	if (!this.info) {
  		this.apiService.getInfo().subscribe((response) => {
	      this.info = response;
	      localStorage.setItem('info', JSON.stringify(this.info));
	    });
	  }
  }
}

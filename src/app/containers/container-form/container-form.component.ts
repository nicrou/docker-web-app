import { Component } from '@angular/core';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';
import { ContainerConfigWrapper }    from '../../models/container-config-wrapper';

@Component({
  selector: 'app-container-form',
  templateUrl: './container-form.component.html',
  styleUrls: ['./container-form.component.scss']
})
export class ContainerFormComponent {
  constructor(private apiService: ApiService, private router: Router) { }

  model = new ContainerConfigWrapper('');
  images: string[] = [];

  submitted = false;

  onSubmit() { 
  	this.submitted = true; 
  	this.apiService.createContainer(this.model).subscribe(data  => {
      console.log("POST Request is successful ", data); 
      this.router.navigate(['/containers']);
    }, error  => {
      console.log("Error", error);
    })
  }

  ngOnInit() {
  	this.apiService.getImages().subscribe((response: Array<any>) => {
      this.images = response.map((o) => { return o.RepoTags[0].substr(0, o.RepoTags[0].indexOf(':')) });
    });
  }

  // TODO: Remove this when we're done
  // get diagnostic() { return JSON.stringify(this.model); }
}
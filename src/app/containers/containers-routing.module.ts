import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ContainerListComponent }    from './container-list/container-list.component';
import { ContainerAnalyticsComponent } from '../containers/container-analytics/container-analytics.component';
import { ContainerFormComponent } from '../containers/container-form/container-form.component';

const containersRoutes: Routes = [
  { path: 'containers',  component: ContainerListComponent },
  { path: 'containers/create',  component: ContainerFormComponent },
  { path: 'containers/:id',  component: ContainerAnalyticsComponent }

];

@NgModule({
  imports: [
    RouterModule.forChild(containersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ContainersRoutingModule { }
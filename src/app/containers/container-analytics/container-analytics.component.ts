import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../api.service';

import { timer, Observable, Subject } from 'rxjs';
import { switchMap, takeUntil, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-container-analytics',
  templateUrl: './container-analytics.component.html',
  styleUrls: ['./container-analytics.component.scss']
})
export class ContainerAnalyticsComponent implements OnInit {
  private container = null;
	public containerId;

  constructor(private apiService: ApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.containerId = this.route.snapshot.paramMap.get('id')
    this.container = timer(0, 5000).pipe(
    switchMap(() => this.apiService.inspectContainer(this.containerId))).
    subscribe((result) => {this.container = result})
    
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerAnalyticsComponent } from './container-analytics.component';

describe('ContainerAnalyticsComponent', () => {
  let component: ContainerAnalyticsComponent;
  let fixture: ComponentFixture<ContainerAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

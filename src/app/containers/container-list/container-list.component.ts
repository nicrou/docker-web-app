import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-container-list',
  templateUrl: './container-list.component.html',
  styleUrls: ['./container-list.component.scss']
})
export class ContainerListComponent implements OnInit {
  containers; showActive: number = 1;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  	this.apiService.getContainers(this.showActive).subscribe((response) => {
      // console.log(response)
      this.containers = response;
    });
  }

  refetch(e: any) {
    this.apiService.getContainers(this.showActive).subscribe((response) => {
      this.containers = response;
    });
  }

  toggleActive() {
    this.showActive = this.showActive === 1 ? 0 : 1
    this.apiService.getContainers(this.showActive).subscribe((response) => {
      // console.log(response)
      this.containers = response;
    });
  }
}
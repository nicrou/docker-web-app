import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-container-detail',
  templateUrl: './container-detail.component.html',
  styleUrls: ['./container-detail.component.scss']
})

export class ContainerDetailComponent implements OnInit {
  @Input() container: any; id;
  @Output() refetch: EventEmitter <any> = new EventEmitter <any> ();
  running;

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
  	this.container.Id = this.container.Id.substring(0, 12);
  }

  ngOnChanges(changes) {
    this.running = changes.container.currentValue.State == 'running' ? true : false;
  }

  start() {
  	this.apiService.startContainer(this.container.Id).subscribe((response) => {
  		this.refetch.emit(response);
  	})
  }

  stop() {
  	this.apiService.stopContainer(this.container.Id).subscribe((response) => {
  		this.refetch.emit(response);
  	})
  }

  delete() {
  	this.apiService.deleteContainer(this.container.Id).subscribe((response) => {
  		this.refetch.emit(response);
  	})
  }

  view() {
    this.router.navigate(["/containers", this.container.Id])
  }
}

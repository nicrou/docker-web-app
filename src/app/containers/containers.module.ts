import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';

import { ContainerListComponent }    from './container-list/container-list.component';
import { ContainerDetailComponent } from './container-detail/container-detail.component';
import { ContainerFormComponent } from './container-form/container-form.component';

import { ContainersRoutingModule } from './containers-routing.module';
import { MaterialDesignModule } from '../material-design/material-design.module';
import { ContainerAnalyticsComponent } from './container-analytics/container-analytics.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ContainersRoutingModule,
    MaterialDesignModule,
    FlexLayoutModule
  ],
  declarations: [
    ContainerListComponent,
    ContainerDetailComponent,
    ContainerFormComponent,
    ContainerAnalyticsComponent,
  ]
})
export class ContainersModule {}

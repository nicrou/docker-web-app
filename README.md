# DockerWebApp
Sample Angular 8 playground project that implements a rather crude web application UI for basic docker tasks, mostly regarding containers' CRUD operations. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

### Docker
A Dockerfile is included so if you are using docker just build the image and run it. 

For example `sudo docker build --tag=docker-web-app:dev .` and then run `sudo docker run -p 4200:4200 docker-web-app:dev` will start the application at port 4200.

### Manually
If you want to build it manually the application was developed with node 10.16.3 and Angular 8.2.4, install the dependencies with `npm install` and serve the application `ng serve` given that Angular is installed of course.

#### Notice

Additionally the dockerd must be configured for API usage and enable the --api-cors-header. The Dockerfile should take care of it if you decide to run with docker however it is untested on a machine that was not already configured. 

In any case the process to enable it manually, according of course with your docker installation it would be for example by adding something like `/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376 --api-cors-header="*"` to the dockerd entrypoint. You can create an override file such as `/etc/systemd/system/docker.service.d/override.conf`. Learn more about it [here](https://success.docker.com/article/how-do-i-enable-the-remote-api-for-dockerd)



